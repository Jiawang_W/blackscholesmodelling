from math import sqrt, log, exp, erf
from random import gauss
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os
from scipy import stats

S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility
Nsteps = 100  # Number or steps in MC

### The first is the csv file
data=pd.read_csv("../Data/BlackScholes.csv", sep=';')
print(data)


#### The second is the analytical price
# Bs formula and results
def BS_call(s0,ss,r,q,vol,T):
    d1 = (log(s0 / ss) + (r + 0.5 * vol**2-q) * T) / (vol * sqrt(T))
    d2 = (log(s0 / ss) + (r - 0.5 * vol**2-q) * T) / (vol * sqrt(T))
    call = s0 * stats.norm.cdf(d1) - ss * exp(-r * T) * stats.norm.cdf(d2)
    return call
print(BS_call(100,80,0.01,0.02,0.2,1))
BS_value=[]
for i in range(50,150):
    BS_value.append(BS_call(S0,i,r,q,vol,T))

# Fill the table
analytical_price=pd.DataFrame({"STRIKES": strikes, "OPTIONVAL":BS_value})
print(analytical_price)


### The third is the MC
I=1000 #the total number of simulations
#Us monte carlo simulation to calculate the stock price
dt=T/Nsteps
MC_S=[]
for i in range (I):
    path=[]
    for t in range(Nsteps):
        if t==0:
            path.append(S0)
        else:
            z=gauss(0.0,1.0)
            St=path[t-1]*exp((r-q-0.5*vol**2)*dt+vol*sqrt(dt)*z)
            path.append(St)
    MC_S.append(path)

# Fill the table
MC_value=[]
for i in range(50,150):
    MC_value.append(exp(-r*T)*sum([max(path[-1]-i,0) for path in MC_S])/I)
MC_price=pd.DataFrame({"STRIKES": strikes, "OPTIONVAL":MC_value})
print(MC_price)

plt.plot(data['STRIKES'],data['OPTIONVAL'],'g+')
plt.plot(analytical_price['STRIKES'],analytical_price['OPTIONVAL'],'b-')
plt.plot(MC_price['STRIKES'],MC_price['OPTIONVAL'],'r+')
plt.show()

###123